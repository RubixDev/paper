\newpage
\section{Characteristics of the rush Programming Language}

For this paper, we have developed and implemented a simple programming language called rush\footnote{Capitalization of the name is intentionally omitted}.
The language features a \emph{static type system}, \emph{arithmetic operators}, \emph{logical operators}, \emph{local and global variables}, \emph{pointers}, \emph{if-else expressions}, \emph{loops}, and \emph{functions}.
In order to introduce the language, we will now consider the code in listing \ref{lst:rush_fib}.

\Lirsting[caption={Generating Fibonacci Numbers Using rush}, label={lst:rush_fib}, float=H]{listings/fib.rush}

This rush program can be used to generate numbers included in the Fibonacci sequence.
In the code, a function named \texttt{fib} is defined using the \texttt{fn} keyword.
This function accepts the parameter \texttt{n}, it denotes the position of the number to be calculated.
Since \texttt{int} is specified as the type of the parameter, the function may be called using any integer value as its argument.
However, the constraint $n \in \mathbb{N}$ must be valid in order for this function to return the correct result\footnote{Assuming the function should comply with the original Fibonacci definition}.
In this example, the \texttt{main} function calls the \texttt{fib} function using the natural number 10.
In rush, every valid program must contain exactly one \texttt{main} function since program execution will start there.
Even though rush has a \texttt{return} statement, the body of the \texttt{fib} function contains no such statement.
This is because blocks like the one of the function \texttt{fib} return the result of their last expression.
The if-else construct must therefore also be an expression since it represents the last entity in the block, and it is not followed by a semicolon.
In this example, if the input parameter \texttt{n} is less than 2, it is returned without modification.
Otherwise, the function calls itself recursively in order to calculate the sum of the preceding Fibonacci numbers $n - 2$ and $n - 1$.
Therefore, the result value of the entire if-expression is calculated by using one of the two branches.
Since the if-else construct is also an expression, there is no need for redundant \texttt{return} statements.
In line 2, the \texttt{exit} function is called.
However, this function is not defined anywhere.
Nevertheless, the code still executes without any errors.
This is due to the fact that the \texttt{exit} function is a \emph{builtin} function as it is used to exit a program using the specified exit code.

\def\commit{}
In the git commit \rushCommit, the entire rush project includes
\input "|tokei ./deps/rush -o json | jq '.Total.code'" lines of source code\footnote{Blank lines and comments are not counted}.
On the first sight, this might seem like a large number for a simple programming language.
However, the rush project includes a lexer, a parser, a semantic analyzer, five compilers, one interpreter, and several other tools like a language server for IDE support.
In the rush project, most of the previously presented stages of compilation are implemented as their own individual code modules.
This way, each component of the programming language can be developed, tested, and maintained separately.
