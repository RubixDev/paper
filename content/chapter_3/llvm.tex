\section{Using LLVM for Code Generation}

LLVM is a software project intended to simplify the construction of a compiler generating highly-performant output programs.
It originally started as a research project by \emph{Chris Lattner} for his master's thesis at the University of Illinois at Urbana-Champaign \cite{Lattner:MSThesis02}.
Since then, the project has been widely adopted by the open source community.
In 2012, the project was rewarded the \emph{ACM Software System Award}, a prestigious recognition of significant software which contributed to science.
From the point where popularity of the framework grew, it was renamed from \emph{Low Level Virtual Machine} to the acronym it is known by today.
Today, it can be recognized as one of the largest open source projects~\cite[preface]{Cardoso_Lopes2014-jt}.
Among many other projects, the Rust programming language depends on the LLVM compiler in order to generate its target-specific code \cite[p.~373]{McNamara2021-hz}.
Furthermore, the \emph{Clang} C / C++ compiler uses LLVM as its code generating backend~\cite[preface]{Hsu2021-ez}.
Therefore, production ready compilers for popular programming languages have been implemented using the LLVM framework.
Besides open-source projects, many companies also use LLVM in their commercial software.
For instance, since 2005, Apple has started incorporating LLVM into some of its products~\cite[pp.~11-15]{Fandrey}.
A recent example of software developed by Apple which uses LLVM is the \emph{Swift} programming language which is mainly used for developing IOS apps \cite[preface]{Hsu2021-ez}.

\subsection{The Role of LLVM in a Compiler}

In a compiler system using LLVM, it is responsible for generating target-specific code.
Furthermore, LLVM is known for performing very effective optimizations during code generation so that the translated program runs faster at runtime and uses less memory.
In order to use LLVM, the system provides and API which is usable by earlier steps of compilation.
Typically, a compiler frontend must only analyze the source program to create an AST.
Therefore, LLVM represents the \emph{back end} of a compiler system.
Then, the AST is traversed and the API of LLVM is used to construct an intermediate representation of the program so that the system can understand it.
Next, LLVM compiles the input program to an arbitrary target architecture.
As of today, LLVM features many target architectures so that a compiler designer does not have to worry about portability of the output program \cite[preface]{Hsu2021-ez}.
Listing~\ref{fig:compilation_steps_llvm} shows how LLVM integrates into the previously discussed steps of compilation.

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[node distance=3mm and 1cm, inner sep=3mm]
		\node (syntactic_analysis_text) [inner sep=0] {syntactical analysis};
		\node (lexical_analysis) [rec, below=of syntactic_analysis_text] {lexical analysis};
		\node (syntactic_analysis) [rec, fit={(syntactic_analysis_text) (lexical_analysis)}] {};

		\node (semantic_analysis) [rec, right=of syntactic_analysis] {semantic analysis};
		\draw [arrow] (syntactic_analysis) -- (semantic_analysis);

		\node (llvm) [rec, right=of semantic_analysis] {LLVM};
		\draw [arrow] (semantic_analysis) -- (llvm);
	\end{tikzpicture}
	\caption{Steps of Compilation When Using LLVM}\label{fig:compilation_steps_llvm}
\end{figure}

\subsection{The LLVM Intermediate Representation}

The intermediate representation (\emph{IR}) represents the source program in a low-level but target-independent way.
Through the use of the LLVM intermediate representation, high-level type information is preserved while the benefits of a low-level representation are introduced.
This allows LLVM to perform significant more aggressive optimizations at compile time compared to other compiler solutions.
Furthermore, LLVM communicates a lot of information to the linker.
As a result of this, many so-called \emph{link time optimizations} can be achieved which are not present in most other compilers \cite[p.~5]{Lattner:MSThesis02}.
Therefore, programs compiled using LLVM as the backend will often run significantly faster due to the many aggressive optimizations introduced by the system.

LLVM provides many APIs for interacting with the IR in memory, so that it can be created by a compiler frontend without it being written onto a file.
The official API for LLVM is for C++ and C.
However, there are many unofficial bindings, such as for Rust, Go, or Python.
For instance, a compiler frontend written in Rust can leverage LLVM, although the system is written in C++.
A program represented using the IR always obeys the following hierarchy:

\begin{itemize}
	\item The top most hierarchical structure is the so-called \emph{module}.
	      It represents the current file being compiled.
	\item Each module contains several \emph{functions}.
	      Often, each function in the source program is represented using a function in the LLVM IR.
	\item Each function contains several \emph{basic blocks}.
	      Such a basic block contains a sequence of instructions.
	      Blocks always have to be terminated using a jumping or returning instruction.
	      However, a block must never be terminated twice.
	\item As mentioned above, each basic block contains a sequence of \emph{instructions}.
	      Each instruction holds a semantic meaning and represents a part of the source program.
\end{itemize} \cite[p.~211-213]{Hsu2021-ez}.

The IR provides a low-level enough representation in order to allow optimizations in the early stages of compilation.
However, due to the high-level type information contained in a program represented using the IR,
LLVM is able to perform many aggressive optimizations on the IR during later stages of compilation.
This way, LLVM can communicate a lot of information to the linker which can then use this information for link-time optimizations.
The virtual instruction set of LLVM is therefore designed as a low-level representation with high-level type information.
This instruction set represents a virtual architecture which is able to represent most common types processors.
However, the IR avoids machine specific constraints like register-count or low-level calling conventions.
The virtual architecture provides an infinite set of virtual registers which can hold the value of primitives like \emph{integers}, \emph{floating-point numbers}, and \emph{pointer}.
All registers in the IR use the \emph{SSA}\footnote{Short for \enquote{static single assignment}, widely used in optimizing compilers} form in order to allow more optimizations.
In order to enforce the correctness of the type information included in the IR,
the operands of an instruction all obey LLVM's type rules~\cite[p.~14-17]{Lattner:MSThesis02}.

In order to understand how the LLVM IR represents a program, we now consider the calculation of Fibonacci numbers again.
For reference, the rush program used in this example can be found in Listing~\ref{lst:rush_fib} on page~\pageref{lst:rush_fib}.
The code in Listing~\ref{lst:llvm_fib} displays LLVM IR representing this rush program.
The IR was generated by the LLVM targeting rush compiler\footnote{Generated in Git commit \rushCommit, automatically built with this document}.

\Lirsting[ranges={5-30}, raw queries=true, caption={LLVM IR Representation of the Program in Listing~\ref{lst:rush_fib}}, label={lst:llvm_fib}, float=H]{listings/generated/fib.ll}

The code displayed in the snipped is part of a LLVM module.
In the lines 5 and 29, functions are defined using the \texttt{declare} keyword.
It is apparent that the functions in the LLVM module represent the functions from the source rush program.
Furthermore, if we examine the signature of the \texttt{fib} function in line 5 of the IR,
it becomes apparent that the function returns an \texttt{i64}.
In rush, each \texttt{int} can hold 64-bit signed numbers, therefore the \texttt{i64} LLVM type represents the rush \texttt{int} type.
Furthermore, we can observe that the function takes an i64 parameter named \texttt{\%0}.
This parameter represents the \texttt{n} parameter in our rush source program.

In line 6, the start of the \enquote{entry} block of the \texttt{fib} function is declared using the block's name followed by a colon.
Since LLVM can perform more optimizations on variables if they are declared in the \texttt{entry} block of a function,
out compiler uses the \texttt{entry} block solely for variable declarations.
In line 7, the block is terminated using the \texttt{br}\footnote{Short for \enquote{branch}} instruction.
This instruction jumps to the beginning of the block specified in its operand.
In this case, the target of the jump is the beginning of the \texttt{body} basic block in the same function.
Due to constraints introduced by its internal optimizations, LLVM only allows targeting blocks contained in the same function.

In line 10, the \texttt{icmp slt}\footnote{Short for \enquote{integer compare (signed less than)}} is used in order to compare the runtime value of the parameter \texttt{\%0} to a constant 2.
The boolean result is then saved in the virtual register \texttt{\%i\_lt}.
Here, it becomes apparent that LLVM's virtual registers can be given arbitrary names.
In places where this is possible, our compiler will use names which will make reading the IR easier for humans.
In line 11, another branch-instruction is used.
However, this time, the jump is placed under the condition that the value of \texttt{\%i\_lt} is true.
Here, we can see that LLVM instructions are able to operand on different type of operands depending on what the instruction should do.
Furthermore, the \texttt{else} label is also an operand of the branch-instruction.
This is because conditional jumps in LLVM always require an alternative jump to perform if the condition is false at runtime.
As the names \texttt{then} and \texttt{else} suggest, this branch-instruction presents the essential part of the if-expression in the source program.
If the condition was true at runtime, the instruction would jump to the \texttt{then} block.
However, this block only contains one instruction jumping to the \texttt{merge} block.

In line 17, the \texttt{phi} instruction is used.
These so called $\phi$-nodes are necessary due to the SSA form used in the LLVM IR\@.
In short, a \emph{phi-node} produces a different value depending on the basic block where control came from.
Since the if-construct is an expression in rush, LLVM must know if the result of the \texttt{then} or the \texttt{else} branch is to be used as the result of the entire if-expression.
As a solution to this problem, these phi-nodes associate a value to an origin branch.
In this example, the phi-node yields the value of the parameter \texttt{\%0} (\texttt{n}) if control came from the \texttt{then} block.
In the source program, \texttt{n} should be returned without modification if it is less than 2.
Therefore, the runtime result of the phi-node is \texttt{\%0} if it is less than 2 at runtime.
Otherwise, if control came from the \texttt{else} block, the phi-node's result is taken from the virtual register \Verb|\%i_sum3|.
However, we have not covered where this virtual register is declared.
For this, we consider the instructions in the \texttt{else} block, starting in line 21 with the \texttt{sub} instruction.
In this case, the instruction subtracts 2 from the parameter \texttt{\%0} and saves the result in \Verb|\%i_sum|.
This is done in order to create the argument value for the first recursive call to \texttt{fib}.
Next, the \texttt{call} instruction is used in order to perform the recursive call.
Here, the \Verb|\%i_sum| register is used as an argument to the call-instruction.
The return value of the function call is saved in the \Verb|\%ret_fib| register.
The same behavior is used in order to call \texttt{fib(n - 1)}.
However, in that case, 1 is subtracted from the parameter and saved in \Verb|\%i_sum1|.

Next, the \texttt{add} instruction in line 25 is used in order to calculate the sum of the return values of the recursive calls
This sum is then saved in the virtual register \Verb|\%i_sum3|.
Therefore, this register is used in the phi-node in line 17 so that the result of the recursive calls is used as the result of the if-expression.
Finally, the \texttt{ret} instruction in line 18 is used in order to use the result of the if-expression as the return-value of the function.

Since the \texttt{main} function does not introduce any new concepts, we will omit detailed explanation of its contents.
However, in line 36, the \texttt{unreachable} instruction is used in order to state that it is never executed.
This is necessary because LLVM requires that every basic block is terminated at its end.
The \texttt{exit} function terminates the program using a system call and therefore terminates the basic block.
However, LLVM does not regard call-instructions as diverging and therefore disallows the call to \texttt{exit} as a way to terminate the basic block.
Since LLVM does not know that the \texttt{exit} function terminates program execution, an \texttt{unreachable}  instruction is inserted to communicate a block termination to LLVM.

By considering the example from above, it became apparent that the IR represents many source language constructs in a high-level way.
For instance, function calls can be used without considering the complex rules introduced by low-level calling conventions.
Here, calling and returning from a function can be implemented using very little effort.
Furthermore, virtual registers allow the compiler frontend to omit register allocation entirely.
Lastly, the LLVM IR can subjectively be seen as very readable since registers, basic blocks, and functions may contain custom, human-readable labels.
Moreover, most instructions have a relatively reasonable name which allows readers to guess what the instruction is doing without them reading any LLVM documentation.


\subsection{The rush Compiler Using LLVM}

In order to get acquainted to the LLVM framework practically, we have implemented a rush compiler which uses the framework as its backend.
However, the first problem emerged soon since the LLVM project only provides official C / C++ bindings to be used by other programs.
Nonetheless, the entire rush project is written in the Rust programming language.
Therefore, a third-party Rust wrapper around LLVM is required.
We have settled on using the \emph{Inkwell} Rust crate since it exposes a safe rust API for using LLVM for code generation~\cite{Inkwell2023}.

This compiler uses the annotated AST generated by the semantic analyzer in order to translate it into LLVM IR.
Here, each type of AST node is translated using its own individual function.
For instance, an expression AST node is translated into IR by the \texttt{expression} method of the compiler.
This way, translation of individual AST nodes can be organized in order to increase maintainability.
To understand how this rush compiler leverages LLVM in order to translate programs, we should first consider some implementation details.
The code in Listing~\ref{lst:llvm_cmp_struct} displays the top part of the `\texttt{Compiler}' struct definition.

\Lirsting[ranges={26-29}, caption={Struct definition of the rush LLVM \texttt{Compiler}}, label={lst:llvm_cmp_struct}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

The \texttt{context} field in line 27 represents a container for all LLVM entities including modules.
Next, the \texttt{module} field contains the underlying LLVM module.
In line 29, the \texttt{builder} field contains a helper struct provided by LLVM which allows generation of IR whilst only using in-memory structures.
All the types of the above fields are provided by the Inkwell crate and are therefore used to interact with the framework.
In order to get a deeper understanding of how this compiler works exactly, we will now consider how the program in Figure~\ref{fig:llvm_simple} is translated into IR.

\noindent
\begin{figure}[h]
	\begin{minipage}{.5\textwidth}
		\centering
		\Lirsting[fancyvrb={frame=none}]{listings/simple.rush}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\Lirsting[ranges={5-18}, raw queries=true, fancyvrb={frame=none}]{listings/generated/simple.ll}
	\end{minipage}
	\caption{Translation of a Simple rush Program to LLVM IR}\label{fig:llvm_simple}
\end{figure}

The source program on the left side contains the \texttt{foo} and the \texttt{main} functions.
These functions are declared in the lines 5 and 17 of the output IR.
The \texttt{foo} function takes two parameters (\texttt{n} and \texttt{m}).
It uses the two parameters and calculates their sum in order to use it as the exit code of the program.
In line 18 of the IR, the parameters \texttt{n} and \texttt{m} are added together.
The result of this addition is then used in order to call the \texttt{exit} function.
This function call takes place in line 11 of the IR.
Therefore, the exit code of the program will be 5.

During translation, the compiler first iterates over all declared functions in order to add them to the LLVM module.
Listing~\ref{lst:llvm_main_fn} displays the top part of the method responsible for translating the \texttt{main} function.

\Lirsting[ranges={328-351}, caption={Compilation of the `\texttt{main}' Function Using LLVM}, label={lst:llvm_main_fn}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

In the lines 334--336, the \texttt{main} function is added to the current LLVM module.
Here, the name of the function is specific by the \texttt{fn\_name} variable.
The return type of the function is specified by the \texttt{fn\_type} variable.
In most cases, the return-type of the function is an integer since C libraries can then use the function as its \texttt{main} function.
In cases where the generated code should not depend on C libraries, \Verb|fn_name| will be \Verb|_start| and \Verb|fn_type| will state that the function returns \emph{void}.
Next, the `\texttt{entry}' and `\texttt{body}' block are appended to the newly created function.
Therefore, the main-function now contains these two basic blocks.
In the lines 343--347, the \Verb|curr_fn| field of the compiler is updated.
This field holds information about the current function being compiled.
In line 345, the \Verb|llvm_value| field is of particular importance since all later additions of basic blocks, e.g., during loop compilation require the Inkwell \texttt{FunctionValue}.
How the \Verb|entry_block| field in line 346 is used every time a pointer is declared is explained later.
Using the Inkwell crate, most instructions generated will be automatically appended to the end of the current basic block.
Therefore, the position of the instruction builder is changed to the end of the newly created `\texttt{body}' block.
Since this block contains the beginning of the main-function's body, the \texttt{block} method of the compiler is called in line 351.
In this case, this method first creates a new scope, then compiles all the statements which the block contains.
Lastly, the method attempts to compile the block's optional expression.
If the content of the body of the main-function does not lead to the insertion of more basic blocks,
the `\texttt{body}' block will contain the entire contents of the function after the method call.

In line 2 of the example rush program, the \texttt{main} function calls the \texttt{foo} function using the arguments 2 and 3.
In order to understand how this compiler translates function calls, we will now consider Listing~\ref{lst:llvm_call}.

\Lirsting[ranges={951-954}, caption={Compilation of Call-Expressions Using LLVM}, label={lst:llvm_call}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

The code in Listing~\ref{lst:llvm_call} displays a small part of the \Verb|call_expr| method of the rush LLVM compiler.
This snipped shows the statement inserting the LLVM \texttt{call} instruction.
For this, the \Verb|build_call| method of the builder is called using the target function, call arguments, and the name of the result register.
Since the variable \texttt{func} represents the called function, it was previously declared by looking up the function name in the module.
The \texttt{args} variable is of type \Verb|Vec<BasicMetadataValueEnum>| and therefore represents a list of Inkwell values representing the arguments used for the call.
This variable was also defined previously by iterating over the \texttt{node.args} vector containing expressions.
This vector is contained in the provided AST node representing the call-expression.
Each argument expression is then compiled, and its result is placed into the \texttt{args} output vector.
However, we cannot understand how results of expressions are handled in this compiler without considering Listing~\ref{lst:llvm_exprs}.

\Lirsting[ranges={873-879}, caption={Compilation of Expressions Using LLVM}, label={lst:llvm_exprs}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

The code in Listing~\ref{lst:llvm_exprs} shows the top part of the \texttt{expression} method of this compiler.
When consider the method's signature, it becomes apparent that it uses an `\texttt{AnalyzedExpression}' in order to generate a `\texttt{BasicValueEnum}'.
The return type of the function is of particular importance.
Using Inkwell, most inserted instructions yield a symbolical value at compile time.
This value represents a virtual register which will contain a \emph{value} at runtime of the program.
Therefore, the `\texttt{BasicValueEnum}' returned by the function represents the virtual register holding the result of the expression at runtime.
This way, symbolical values can be used at compile time, thus presenting a high-level abstraction for generating the IR.
The lines 875-879, show how a constant integer expression is compiled.
Here, a constant int value of the \texttt{i64} type is created and transformed into a `\texttt{BasicValueEnum}' which is then used as the method's return value.
For more complex expressions, the \texttt{expression} method invokes other methods which are specialized on this type of expression.
For instance, if an infix-expression like `\texttt{3 * n}' is compiled, this method calls the `\Verb{infix_expr}' method of the compiler, using the current AST node as a call argument.

\Lirsting[ranges={1021-1024}, caption={Compilation of Integer Infix-Expressions Using LLVM}, label={lst:llvm_infix}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

The code in Listing~\ref{lst:llvm_infix} shows a part of the `\Verb|infix_helper|' method which is responsible for compiling parts of infix-expression.
Line 1021 contains the code for inserting the `\texttt{mul}' integer multiplication instruction.
Here, the variables `\texttt{lhs}' and `\texttt{rhs}' are used as arguments for the `\Verb|build_int_sub|' method call.
They too represent virtual registers which will contain the value of the left- and righthand side at runtime.
Furthermore, the string containing `\Verb|i_prod|' specifies the name of the virtual register containing the product of the multiplication performed by the instruction.
In this example, compiling basic integer multiplication has proven to be really simple since only one instruction needs to be inserted.
This simplicity applies to most infix operations performed on integers.
However, compiling mathematical power operations has proven to be more demanding since LLVM does not provide an instruction for performing these operations.
Line 1024 is executed if the method needs to compile such a integer power operation.
In order to mitigate this issue, the `\Verb|__rush_internal_pow|' method is called instead of a method provided by Inkwell.
This method first declares the `\Verb|core::pow|' function in order to call it directly after.
This function implements an algorithm for power operations given an integer base and exponent.
However, this function is implemented in IR directly by hardcoding the required calls to Inkwell into this function.
Therefore, even complex calculations like this one can be implemented even though LLVM does not provide a straight-forward way to accomplish them directly.

In line 6 of the source program, a let-statement is used to declare the mutable variable `\texttt{m}' with the initial value 3.
However, there is never a value assigned to this variable.
This variable is only mutable so that the compiler has to use stack memory for it.
Non-mutable variables are inlined by the compiler in order to save resources during runtime.
In order to understand how the compiler translates let-statements, we will now consider Listing~\ref{lst:llvm_let}.

\Lirsting[ranges={609-624}, caption={Compilation of Let-Statements Using LLVM}, label={lst:llvm_let}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

The code in Listing~\ref{lst:llvm_let} displays the top part of the `\Verb{let_stmt}' method of this compiler.
This method is responsible for compiling let-statements.
In line 610, the initializer expression of the statement is compiled.
The \texttt{rhs} variable then specifies the virtual register which contains the result of the expression at runtime.

The code after the line 613 is only executed if the variable was declared as mutable.
Therefore, in order to present this code in action, the \texttt{m} variable in the source program had to be declared as mutable.
In line 616, the \Verb{alloc_ptr} method is used in order to create a new Inkwell pointer value.
The first argument of the call specifies that the name of the pointer should be identical to the name of the variable.
The second argument passes the type of the initializer expression to the method.
The statement in line 619 is used in order to insert a store instruction.
Here, the instruction should store the value of the initializer expression in the newly created pointer.
Since pointers present a way to use stack memory, also non-pointer variables in the source program are internally compiled to an IR program using pointers.
Finally, in line 622, the newly defined variable is inserted into the current scope of the compiler.
Every variable inside the scope saves its Inkwell value and its type since these fields are required when the variable is used later.
The code in Listing~\ref{lst:llvm_ptr_alloc} shows the \Verb{alloc_ptr} method of the compiler.

\Lirsting[ranges={635-649}, caption={Pointer Allocation in the LLVM Compiler}, label={lst:llvm_ptr_alloc}, float=H]{deps/rush/crates/rush-compiler-llvm/src/compiler.rs}

This method exists in order to create a new Inkwell pointer value.
Like hinted previously, pointers are declared in the \texttt{entry} block of each function in order to allow for more aggressive optimizations.
In line 640, this method places the builder cursor at the end of the entry-block of the current function.
Next, in line 643, a \texttt{alloca} LLVM instruction is inserted.
This instruction is responsible for allocating a new pointer which points to stack memory.
After the instruction has been inserted, the builder position is reset to where it was before the method was called.
Finally, the pointer is returned so that it is usable for other parts of the compiler.

\subsection{Final Code Generation: The Linker}

% - Explain linking
% - Difference `.o` vs binary
% - LLVM's link-time optimizations
% - https://en.wikipedia.org/wiki/Linker_(computing)
% - Linker & loader book: P. 5 - 7
% - Insert figure from p. 7

\TODO{Write this chapter}

\Lirsting[caption={Using LD to link the LLVM output}, label={lst:ld_llvm}, float=H]{listings/invoke_ld.sh}

\subsection{Conclusions}

As a conclusion, implementing a compiler which leverages LLVM presents a lot of advantages.
For instance, the language will be able to support many backend architectures.
Most of the demanding work is being done by LLVM, therefore implementing the compiler will proof to be less difficult and error-prone.
Moreover, LLVM performs a lot of very effective optimizations which would otherwise have to be implemented by the compiler designer.
However, these optimizations often involve a lot of work and are therefore unpractical to implement for simpler languages.
Therefore, LLVM presents a robust, production-ready and scalable backend which is used in real-world compilers.
However, by depending on LLVM, the resulting compiler will often be less portable since cross-compilation still presents an issue if used across programming language boundaries.

Finally, in order to understand how LLVM's optimizations can positively impact application performance at runtime, we will consider the Fibonacci benchmark again.
In this benchmark, the 42nd Fibonacci number is calculated using the program displayed in Listing~\ref{lst:rush_fib} on page~\pageref{lst:rush_fib}.
However, the 10 in line 2 was replaced by a 42.
Running a binary compiled using the rush LLVM compiler took around 1.3 seconds.
However, executing the binary generated using the rush x86\_64 compiler took around 2.17 seconds\footnote{Average from 100 iterations. OS: Arch Linux, CPU: Ryzen 5 1500, RAM: 16 GB}.
Therefore, the program compiled using LLVM ran roughly 1.66 times faster.

\TODO{Difficult: blocks need to be terminated, loops and alloca}

% - Explain what was difficuilt and what was good
% - Benchmark: fib LLVM vs x86_64
