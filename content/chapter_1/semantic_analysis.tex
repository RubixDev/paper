\section{Semantic Analysis}
Before compilation can begin, both the syntax and the semantics of the program have to be validated.
The \emph{semantic analysis} is responsible for validating that the structure and logic of the program complies with the rules of the programming language.
Often, semantic analysis directly follows the syntax analysis since the parser generates the input for the semantic analysis step.

\subsection{Defining the Semantics of a Programming Language}
Often, a programming language is not just defined by its grammar
because the grammar cannot specify how programs should behave.
Therefore, a programming language's behavior is often defined by a so-called \emph{semantic specification}.
This specification often describes how a program should behave during runtime and what semantic rules discriminate a valid program from an invalid one.
Common rules include \emph{type checking}, \emph{context of statements}, or \emph{integer overflow behavior}.
Another example of a semantic rule is that a variable has to be declared before it is used.
Defining the semantic rules of a programming language is often a demanding task
since not all requirements are clear from the beginning.
Because the semantic rules of a programming language can not be defined formally,
a language designer often writes their specification in a natural language, meaning Chomsky type 0.
However, due to the specification being written in a natural language, the specification can sometimes be ambiguous.
Therefore, a well-written semantic specification should avoid ambiguity as much as possible.
Furthermore, this specification is often written in English
due to it being a well-adopted language across several academic fields like computer science.
Since those rules define when a program is valid, they have to be checked and enforced before program compilation can start~\cite[p.~21]{a_practical_guide_compiler_construction_watson_2017}.

\subsection{The Semantic Analyzer}
Because rush shares its semantic rules across all backends,
it would be cumbersome to implement semantic validation in each
backend individually. Therefore, it is rational to implement a separate
compilation step which is responsible for validating the source program's semantics.
Among other checks, the so-called \emph{semantic analyzer} \footnote{Later referred to as \enquote{analyzer}} validates types
and variable references whilst performing type annotations. The last aspect is
of particular importance since all compiler backends rely on type information at
compile time. In order to obtain type information, the abstract syntax
tree of the source program must be traversed, performing numerous other checks
during the process. The analyzer behaves exactly like this. In order to
preserve a clear boundary between the individual compilation steps, the parser
only validates the program's syntax without performing further validation.
Therefore, the analyzer traverses the abstract syntax tree previously generated
by the parser.

In order to highlight why type information is often required at compile time, we will consider Listing~\ref{lst:analyzer-test}.
The code in this listing displays a basic rush program calculating the sum of two integers and uses the result as its exit code.
In this example, the exit code of the program will be 5.

\Lirsting[caption={A rush Program Which Adds Two Integers}, label={lst:analyzer-test}, float=H]{listings/simple.rush}

In this example, the analyzer will first check if the program contains a \texttt{main} function.
If this is not the case, the analyzer rejects the program because it violates rush's semantic specification.
Furthermore, the analyzer checks that the \texttt{main} function takes no parameters and returns no value. In this
example, there is a valid \texttt{main} function which complies with the previously listed constraints. Now, the analyzer traverses the
function body of the \texttt{main} function. First, the analyzer examines the statements in the lines two and three.
Since \texttt{let} statements are used to declare
new variables, the analyzer will add the variables \texttt{two} and \texttt{three} to its
current scope. However, unlike an interpreter, the analyzer does not insert the
variable's value into its scope. Instead of the concrete values, the analyzer
only considers the types of expressions. Therefore, in this example, the
analyzer remembers that the variables \texttt{two} and \texttt{three} store integer values.
This information will become much more useful when we consider line 4. Here, the
analyzer checks that the identifiers \texttt{two} and \texttt{three} refer to valid variables.
Just like most other programming languages, rush does not allow the addition
of two boolean values for example. Therefore, the analyzer checks that the operands
of the \texttt{+} operator have the same type and that this type is valid in additions. % TODO: revise this paragraph?
Because this validation requires information about types, the analyzer accesses
its scope when looking up the identifiers \texttt{two} and \texttt{three}. Since those names
were previously associated with the \texttt{int} type, the analyzer is now aware of the
operand types and can check their validity. In this case, calculating the
sum of two integers is legal and results in another integer value. Since rush's
semantic specification states that the \texttt{exit} function requires exactly one
integer parameter, the analyzer has to check that it is called correctly.
Furthermore, the analyzer validates all function calls and declarations, not
just the ones of builtin functions. Since the result of the addition is also an
integer, the analyzer accepts this program since both its syntax and semantics
are valid.

As indicated previously, most compilers require type information whilst
generating target code. For simplicity, we will consider a fictional compiler
which can compile both integer and float additions. However, the fictional
target machine requires different instructions for addition depending on the
type of the operands. For instance, integer addition uses the \texttt{intadd} instruction
while float addition uses the \texttt{floatadd} instruction. Here, type ambiguity would
cause difficulties. If there was no semantic analysis step, the compilation step would
have to implement its own way of determining the types of the operands at
compile time. However, determining these types requires a complete
tree-traversal of the operand expressions. Due to the recursive design of the
abstract syntax tree, implementing this tree-traversal would require a
significant amount of source code in the compiler. However, the implementation
of this algorithm would be nearly identical across all of rush's compiler
backends. Therefore, implementing type determination in each backend
individually would enlarge the compiler source code, thus making it harder to
understand. Since code duplication is considered inelegant, outsourcing this
algorithm into a separate component is likely the best option. As a result of
this, the analyzer implements such a tree-traversal algorithm for
determining the types of subtrees. Because of the previously mentioned reasons,
rush's semantic analyzer also annotates the abstract syntax tree with type
information so that it can be utilized by later steps of compilation.

In order to obtain a deeper understanding of how the analyzer works, we will now
consider parts of its implementation and how they behave when analyzing the
example from above. However, before we can examine how the analyzer's implementation behaves,
we should first highlight which attributes play a vital role in the analyzer.

\Lirsting[ranges={12-26}, caption={Attributes of the \texttt{analyzer} struct}, label={lst:analyzer_attr}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

Listing~\ref{lst:analyzer_attr} displays the struct fields of the semantic analyzer.
The field \texttt{diagnostics} contains a list of diagnostics.
A diagnostic is a struct which represents a message, is intended to be displayed to the user of the compiler.
Each diagnostics has a severity, such as \emph{warning} or \emph{error} for instance.
After the analyzer has finished the tree-traversal, all diagnostics are displayed in a user-friendly manner.
An example for diagnostic messages can be found in Listing~\ref{lst:analyzer_imutable_err}.
The field \texttt{curr\_func\_name} saves the name of the current function.
Moreover, the field \texttt{functions} in line 13 associates a function name to the function's signature.
Therefore, if a function is called at a later point in time, the analyzer checks if the function exists and can compare if the arguments match the declared parameters.
The \texttt{scopes} field in line 16 is responsible for managing variables.
In rush, blocks using braces (\texttt{\{\}}) create new scopes.
If the analyzer enters such a block, a new scope is pushed onto the \texttt{scopes} stack.
Each scope maps a variable identifier to some variable-specific data.
For instance, the analyzer keeps track of variable types, whether variables have been used later, if they are mutated, and the location of where they were declared.
By saving this much information about each declared variable, the analyzer can produce very helpful and accurate error messages or warnings.
Such an error message is displayed in Listing~\ref{lst:analyzer_imutable_err}, it occurs when another value is assigned to an immutable variable.

\Lirsting[ansi=true, caption={Output When Compiling an Invalid rush Program}, float=h, label={lst:analyzer_imutable_err}]{listings/non_mut_variable_error.txt}

Furthermore, the \texttt{loop\_count} field is used to validate the uses of the \texttt{break} and \texttt{continue} statements.
Because these statements are only valid inside loop bodies, the value of \texttt{loop\_count} must be $> 0$ when the analyzer encounters such a statement.
This counter is incremented as soon as the analyzer begins traversal of a loop body.
After the analyzer has traversed the loop's body, the counter is decremented again.
Due to this design, nested loops do not cause issues while the validity of the above statements can be guaranteed.

Now that important attributes have been highlighted, we can now consider the example from above again.
First, the analyzer traverses and analyzes all functions and their bodies.
For every rush function, the analyzer invokes an internal method responsible for validating functions.
Among other tasks, this method sets the \texttt{curr\_func\_name} field to the name of the current function and
inserts a new entry into the \texttt{functions} hashmap, associating the function's name with its signature.
Because a \texttt{main} function is mandatory in every rush program,
the analyzer simply checks that the \texttt{functions} hashmap contains an entry for the \texttt{main} function.
Naturally, this lookup is performed after all functions have been analyzed since the \texttt{main} function can then exist in the hashmap.
The code in listing \ref{lst:analyzer_sig_main} shows how one part of validating the \texttt{main} function's signature works.

\Lirsting[ranges={404-419}, caption={Analyzer Validating the Signature of the `\texttt{main}' Function}, label={lst:analyzer_sig_main}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

This code is only executed if the \texttt{curr\_func\_name} field of the analyzer holds the value \texttt{main}.
Therefore, these special checks are only performed for the \texttt{main} function.
The if-clause in line 405 checks if the node's \texttt{params} vector contains any items.
If this is the case, the analyzer generates an error message describing the issue.
However, we have not yet explained how error handling in the analyzer works.
When examining the signatures of some of the analyzer's methods, it becomes obvious that these methods do not return errors.
Instead, the \texttt{self.error} method is invoked.
This method requires the type of the error to be reported, its message, where it occurred and optional hints to display.
The error is reported because this method pushes a new \texttt{Diagnostic} struct into the \texttt{diagnostics} vector.

During the traversal of the main function's body, the analyzer encounters two \texttt{let} statements in line 2 and 3.
For analyzing this type of statements, the \texttt{let\_stmt} method is invoked.

\Lirsting[ranges={612-617}, caption={Beginning of the \texttt{let\_stmt} Method}, label={lst:analyzer_let_stmt}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

First, the initializing expression of the let-statement is analyzed in order to obtain information about its result data type.
After the subtree of the expression has been traversed and analyzed, its data type is now known.
The analyzer now inserts a new entry for the variable's name (e.g. \texttt{two}) into its current scope.
This variable insertion is displayed in Listing~\ref{lst:analyzer_scope_insertion}.

\Lirsting[ranges={644-657}, caption={Insertion of a Variable Into the Current Scope}, label={lst:analyzer_scope_insertion}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

As seen in line 652, the insertion includes the variable's span.
Since the span includes the location of where the variable was declared,
it can later be used in error messages like the one displayed in Listing~\ref{lst:analyzer_imutable_err}.
Furthermore, the inserted information includes whether the variable was declared as mutable.
If a variable is mutable, it can be reassigned to.
Because the variable in our example program was not declared as mutable,
the error seen in Listing~\ref{lst:analyzer_imutable_err} was generated as a result.
By default, the variable is set to non-mutated (line 655).
Most rush compiler backends use this information about actual mutation in order to implement some optimizations.
What strikes the eye is that the insertion happens as a condition inside an if-clause.
If the insertion returns \texttt{true}, the variable's name was already present in the current scope and its associated data has now been overwritten.
This overwriting of variables is called \emph{variable shadowing}.
Here, the analyzer should display some additional hints or warnings, depending on whether the shadowed variable has been referenced before it was shadowed.
If this was not the case, the analyzer will generate an error message informing the user about an unused and therefore redundant variable.
Among the previously mentioned data, the insertion includes the variable's data type which was obtained by prior analysis of the expression.
However, we have not yet explained how type determination and annotation works in the analyzer.
In order to get an understanding of how the analyzer determines types of variables, we must consider how expressions are traversed.
The code in Listing~\ref{lst:analyzer_expr} is part of the method responsible for analyzing expressions.

\Lirsting[ranges={1007-1013}, caption={Analysis of Expressions During Semantic Analysis}, label={lst:analyzer_expr}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

The \texttt{node} parameter specifies the expression node generated by the parser,
it does not contain any type information since it is yet to be analyzed.
It is also apparent that the method returns a value of the type \texttt{AnalyzedExpression},
which represents an analyzed and annotated expression.
Therefore, this method consumes a non-analyzed expression and transforms it into an analyzed version of itself.
In this function, the recursive tree-traversal algorithm used in the analyzer is clearly visible.
For instance, if the current expression is a block-expression (line 1009), the responsible method is called.
Since rush allows blocks which contain expressions, a block containing another block is a legal construct.
Therefore, it is possible that the \texttt{expression} method calls itself multiple times recursively.
Most of the other tree-traversing methods implement a similar recursive behavior as most AST nodes may contain themselves.
For simple types of expressions like integers or floats, further analysis is omitted.
Since these types of expressions are constant, the method can directly return an analyzed version of the expression.
Since we have now explained how tree traversal and analysis works in general, the question of how types are accessed and saved in the annotated syntax tree remains.

\Lirsting[ranges={129-146}, caption={Obtaining the Type of Expressions}, label={lst:expr_type_impl}, float=H]{deps/rush/crates/rush-analyzer/src/ast.rs}

The code in Listing~\ref{lst:expr_type_impl} shows how the type of any analyzed expression can be obtained.
For constant expressions like \verb|AnalyzedExpression::Int(_)|, the determination of its type is straight-forward.
Here, the \texttt{result\_type} method returns \verb|Type::Int(0)|.
In this implementation, the \texttt{Type} enum saves a count which specifies the amount of pointer indirection.
For instance, the rush type \texttt{**int} is represented as \verb|Type::Int(2)| because there are two levels of pointer indirection.
However, if the method is called on a constant integer expression, the resulting level of pointer indirection is zero.
Therefore, this method is able to return the types of simple constant expressions with no additional effort.
For more complex constructs like if-expressions,
the corresponding analyzed AST node saves its result type directly.
For instance, during analysis of block expressions,
the responsible function checks if the block contains a trailing expression.
If this is the case, the result type of the block expression is identical to the one of its trailing expression.
This way, the analyzer is able to get type information about each node of the tree, assuming that it has been analyzed previously.
In the case of a semantically malformed program,
the analyzer must somehow continue the tree traversal.
Otherwise, only one error could be reported at a time since every traversing method could return a potential error which would terminate the tree traversal.
To mitigate this issue, we have implemented the \texttt{Unknown} type.
If the analyzer encounters a type conflict where one of the conflicting types is \emph{unknown},
it does not report another error since the unknown type was caused solely by a previous error.
Therefore, errors do not cascade, meaning that an undeclared variable will not cause another type error.

After the let-statements, the \texttt{exit} function is called.
Here, the analyzer calls the \texttt{call\_expr} function which is responsible for analyzing the validity of function calls.
First, the call parameter expressions are analyzed.
Therefore, the expression \texttt{two + three} is traversed before further analysis can proceed.
Since the identifiers on the left- and right hand side have been declared by the two let-statements previously,
obtaining their data types merely involves a lookup of the identifier names inside the current scope's hashmap.
If an unknown variable was provided, the lookup in the hashmap would yield no value, thus causing an error message to be generated at this point.
Because the type of the invalid variable is unknown since it was not declared, the \texttt{Unknown} type would be used here in order to prevent cascading errors.
Because the values of the two variables should be added, the method \texttt{infix\_expr} is called.
This method validates several constraints.
For instance, the operands must both be of the same type.
Here, both operands are integers, thus complying with the specification.
Therefore, the analyzer accepts this infix-expression and is now aware that it yields another integer.
After the infix-expression's result type has been determined, it is saved in its own \texttt{result\_type} struct field.
Infix-expressions are a classical example for tree nodes which save their result type as a struct field on their own.
Therefore, the type of infix-expressions can be accessed like it is displayed in line 140 of the Listing~\ref{lst:expr_type_impl}.
Now that the analysis of the argument expression has completed, its compatibility with the declared parameter must be validated.

\Lirsting[ranges={1822-1827}, caption={Validation of Argument Type Compatibility in the Analyzer}, label={lst:analyzer_call_exit}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

The listing \ref{lst:analyzer_call_exit} shows a part of the \texttt{arg} function which is responsible for validating that a function call argument is compatible with the declared parameter.
In the above example, this means that the \texttt{exit} function is to be called with exactly one integer argument.
This code will produce an error message if the type of the call argument deviates from the one of the declared parameter.
Since the example from above presents a syntactically and semantically valid rush program, the analyzer accepts this program and returns its annotated tree.

\subsection{Early Optimizations}

Another task of the analyzer can be to perform early optimizations.
In compiler design, most of the optimizations are often performed with the target machine in mind.
Therefore, the effects of these target-machine dependent optimizations can excel the ones caused by earlier optimizations.
However, it is still rational to perform trivial optimizations, such as constant folding and loop conversion inside the analyzer.
For instance, the rush expression $2 + 3$ evaluates to $5$ during compile time instead of run time.
This evaluation of expressions during compile time is referred to as \emph{constant folding}.
Constant folding is often used in order to avoid the emission of otherwise redundant arithmetic instructions.
As a result of this, the compiled program will run faster since less computation is being performed when the program is executed.
In order to make such optimization possible, each expression node in the analyzed AST has a method named \texttt{constant}~\cite[p.~54]{wirth_compiler_construction_2005}.

\Lirsting[ranges={148-153}, caption={Method for Determining if an Expression is Constant}, label={lst:expr_constant_impl}, float=H]{deps/rush/crates/rush-analyzer/src/ast.rs}

This method is responsible for determining whether an expression is constant.
The method returns \texttt{true} if its expression is a constant integer, float, boolean or char.
Other types of expression, such as a call-expression cannot be constant since such a function call may cause side effects which cannot be determined during compile time.
This method is vital for constant folding since both the left- and right-hand side of infix-expressions need to be constant in order to allow compile-time evaluation.

Among other optimizations implemented in the analyzer, loop transformation can also have a positive effect on the program's performance during runtime.
The top listing displays part of a rush program which uses a \texttt{while} loop even though a \texttt{loop} would be faster.
The other listing displays the same algorithm implemented using the faster \texttt{loop}.

\Lirsting[caption={Redundant \texttt{while} Loop Inside a rush Program}]{listings/constant_true_while.rush}
\Lirsting[caption={Faster Loop Algorithm Implemented in rush}]{listings/faster_loop.rush}

The \texttt{loop} implementation is more efficient since the condition check is omitted before each iteration.
Because the \texttt{while} loop checks that its head condition is true before it starts the next iteration,
the \texttt{while} loop will run slower than the \texttt{loop} in this example.
However, this is only the case because the condition of the loop is a constant \texttt{true}.
Therefore, using a condition which is always true is redundant and should therefore be omitted.
If the analyzer detects such a scenario after a while-loop was analyzed, the output node will be converted into a conventional loop.
Detection of this scenario is implemented in line 855 of Listing~\ref{lst:analyzer_loops}.

\Lirsting[ranges={851-865}, caption={Loop Transformation in the Analyzer}, label={lst:analyzer_loops}, float=H]{deps/rush/crates/rush-analyzer/src/analyzer.rs}

Another scenario in which a \texttt{while} loop can be restructured occurs if the condition always evaluates to \texttt{false}.
This example is displayed in the listing below.

\Lirsting[caption={\texttt{while} Loop Inside a rush Program Which Never Iterates}]{listings/constant_false_while.rush}

Since the loop in the above listing never iterates, it is completely redundant and can therefore be omitted entirely.
This scenario is detected in line 853 of Listing~\ref{lst:analyzer_loops}.
This optimization improves runtime efficiency by a small amount since the code performing the very first condition check will not be compiled into the output program.
Furthermore, the resulting output code will also be of slightly smaller size since the entire loop compilation can be omitted.
Therefore, implementing such trivial optimizations can significantly contribute to a more efficient output program.
However, compiler writers often implement significantly more of those early optimizations than the ones presented in the two examples from above.
